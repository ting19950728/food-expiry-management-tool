function deleteFoodRecord(id){
 window.location = "/food/delete/" + id;
}

$(document).ready(function(){
 $('input.foodType').change(function(){
  foodTypeCheck();
 });
 foodTypeCheck();
});

function foodTypeCheck(){
 var foodType = $('input.foodType:checked').val();
 if(foodType == 'raw'){
  $('label#expiryDate').hide();
  $('input#expiryDate').attr('disabled', 'disabled').hide();
 } else {
  $('label#expiryDate').show();
  $('input#expiryDate').removeAttr('disabled').show();
 }
}
