var express = require('express');
var router = express.Router();
var ObjectID = require('mongodb').ObjectID;
var foodDB = require('./../lib/foodDB');
var dateChecker = require('./../lib/checkDate');

/* GET users listing. */
router.get('/', function(req, res, next){
 let fdb = new foodDB();
 fdb.selectAll(function(result){
  checkExpired(result);
  res.render('foodRecord', {title: "Food Record Page", records: result});
 });
});

router.post('/', function(req, res, next){
 let fdb = new foodDB();
 let isOrder = req.body.sortOption != 'none';
 let sortOrder = (req.body.sortOrder == 'asc') ? 1 : -1;
 if(isOrder){
   let sortOption = {};
   sortOption[req.body.sortOption] = sortOrder;
   fdb.findSort(new RegExp(req.body.filter_name, 'i'), sortOption, function(result){
    checkExpired(result);
    res.render('foodRecord', {title: "Food Record Page", records: result, keyword: req.body.filter_name});
   });
 } else {
   fdb.findByName(new RegExp(req.body.filter_name, 'i'), function(result){
    checkExpired(result);
    res.render('foodRecord', {title: "Food Record Page", records: result});
   });
 }
});

router.post('/insert', function(req, res, next) {
  let fdb = new foodDB();
  if(req.body.foodType == 'raw'){
    var date = new Date(req.body.buyTime);
    date = date.addDays(7);
    date = date.toLocaleDateString();
    console.log(date);
    req.body.expiry = date.replace('/', '-').replace('/', '-');
  }
  fdb.insert(req.body, function(result){
   if(result > 0 ){
    res.redirect('/food');
   } else {
    res.redirect('/');
   }
  });
});

router.get('/delete/:id', function(req, res, next){
 let fdb = new foodDB();
 fdb.delete({"_id" : new ObjectID(req.params.id)}, function(result){
  if(result > 0){
   res.redirect('/food');
  } else {
   res.redirect('/');
  }
 })
});

module.exports = router;

Date.prototype.addDays = function(days) {
  var date = new Date(this.valueOf());
  date.setDate(date.getDate() + days);
  return date;
}

function checkExpired(result){
  let dc = new dateChecker();
  for(var x in result){
    var expiryDate = new Date(result[x].expiry);
    var isOver = dc.compareDate(expiryDate);
    if(isOver){
      result[x].recordType = 'fine';
    } else {
      result[x].recordType = 'expired';
    }
  }
}