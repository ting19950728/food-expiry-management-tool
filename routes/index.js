var express = require('express');
var router = express.Router();
var dbc = require('./../lib/dbc');
var foodDB = require('./../lib/foodDB');
var dateChecker = require('./../lib/checkDate');

/* GET home page. */
router.get('/', function(req, res, next) {
  let fdb = new foodDB();
  fdb.selectAll(function(result){
    var expiredItem = [];
    var dc = new dateChecker();
    for(var x in result){
      var expiryDate = new Date(result[x].expiry);
      var isOver = dc.compareDate(expiryDate);
      if(!isOver)
       expiredItem.push(result[x].name);
    }
    let expiryAlert = null;
    if(expiredItem.length > 0){
      expiryAlert = "ALERT!\\nFood Item(s) expired!\\nTotal : " + expiredItem.length;
      expiredItem.forEach(function(value){
        expiryAlert += '\\nName : ' + value;
      });
    }
    res.render('index', { 
      title: 'Food expiry management tool',
      menu: [
        {title : "Food Record", target:"/food"}
      ],
      alert: (expiredItem.length > 0),
      expiryAlert : expiryAlert
    });
  });
});

module.exports = router;
