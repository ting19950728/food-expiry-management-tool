const mongodb = require('mongodb');
const Server = mongodb.Server;
var MongoClient = mongodb.MongoClient;
var url = "mongodb://localhost:27017";
var dbname = "femt";

function controller(){
 this.connect = function(callback){
  MongoClient.connect(url, (err, client) => {
   if(err) return console.log(err);
   this.client = client;
   var db = client.db('femt');
   callback(db);
  });
 }

 this.close = function(){
  if(this.client != null)
   this.client.close();
 }
}

module.exports = controller;