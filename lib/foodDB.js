const dbc = require('./dbc');

function foodDB(){
 this.controller = new dbc();
 this.collectionName = "foodRecord";
 this.insert = function(records, callback){
  let collectionName = this.collectionName;
  let controller  = this.controller;
  if(records instanceof Array){
   controller.connect(function(db){
    db.collections(collectionName).insertMany(records, function(err, result){
     if(err) console.log(err);
     callback(result.insertedCount);
     controller.close();
    });
   });
  } else {
   controller.connect(function(db){
    db.collection(collectionName).insertOne(records, function(err, result){
     if(err) console.log(err);
     callback(result.insertedCount);
     controller.close();
    });
   });
  }
 }

 this.findSort = function(keyword, sortOption, callback){
  let collectionName = this.collectionName;
  let controller = this.controller;
  controller.connect(function(db){
   db.collection(collectionName).find({"name" : keyword}).sort(sortOption).toArray(function(err, result){
    callback(result);
    controller.close();
   });
  });
 }

 this.findByName = function(keyword, callback){
  let collectionName = this.collectionName;
  let controller = this.controller;
  controller.connect(function(db){
   db.collection(collectionName).find({"name" : keyword}).toArray(function(err, result){
    callback(result);
    controller.close();
   });
  });
 }

 this.delete = function(obj, callback){
  let collectionName = this.collectionName;
  let controller = this.controller;
  controller.connect(function(db){
   db.collection(collectionName).deleteOne(obj, function(err, result){
    callback(result.deletedCount);
   });
   controller.close();
  });
 }

 this.selectAll = function(callback){
  let collectionName = this.collectionName;
  let controller = this.controller;
  controller.connect(function(db){
   db.collection(collectionName).find({}).toArray(function(err, result){
    if(err) console.log(err);
    callback(result);
    controller.close();
   });
  });
 }
}

module.exports = foodDB;