const request = require('supertest');
const app = require('../app');

describe('App', function() {
  it('has the main page', function(done) {
    request(app)
      .get('/')
      .expect(/Welcome/, done);
  });

  it('has the food record page', function(done){
    request(app)
      .get('/food')
      .expect(/Add new food record/, done);
  })
}); 
